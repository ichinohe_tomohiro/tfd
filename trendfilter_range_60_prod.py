#!/usr/bin/python3
# coding: utf-8
import requests
from datetime import datetime
import time
import numpy as np
import ccxt
import logging
import logging.handlers
import json
import os
import gc
from pprint import pprint

bitflyer = ccxt.bitflyer()
bitflyer.apiKey = ''
bitflyer.secret = ''

backtest_flag = False

logger = logging.getLogger('LoggingTest')
LOG_FILENAME = os.path.splitext(os.path.basename(__file__))[0] + format(datetime.now().strftime("-%Y-%m-%d")) + '.log'
logger.setLevel(logging.DEBUG)
sh = logging.StreamHandler() # コンソール画面出力設定
sh.setLevel(logging.INFO)
logger.addHandler(sh)
efh = logging.FileHandler('trendfilter_doten_error.log') # エラーログファイル出力設定
efh.setLevel(logging.ERROR)
logger.addHandler(efh)
formatter = logging.Formatter('%(asctime)s:%(lineno)d: %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
sh.setFormatter(formatter)
efh.setFormatter(formatter)

JSON_FILE_PATH = os.path.splitext(os.path.basename(__file__))[0] + 'flag.json'

with open(JSON_FILE_PATH, mode = 'r', encoding = 'utf-8') as jr:
    json_txt = jr.read()
    json_txt = str(json_txt).replace("'", '"').replace('True', 'true').replace('False', 'false')
    json_data = json.loads(json_txt)

flag = json_data  # ファイル終端まで全て読んだデータを返す

logger.info('========== trendfilter doten Prod Start! ==========')

#--------設定項目--------

chart_sec = 60           #  １時間足を使用
buy_term =  3             #  買いエントリーのブレイク期間の設定
sell_term = 3            #  売りエントリーのブレイク期間の設定
high_term = 12            #  買いエントリーのブレイク期間の設定
low_term = 12            #  買いエントリーのブレイク期間の設定

filter_VER = "H"           # OFFで無効
volatility_check_term = 150
volatility_check_under = 0.001   # OFFで無効
volatility_check_over = 0.0080
profit_filter_value = 1
#pre_profit_close_value = 1000000

judge_price={
  "BUY" : "high_price",    #  ブレイク判断　高値（high_price)か終値（close_price）を使用
  "SELL": "low_price"      #  ブレイク判断　安値 (low_price)か終値（close_price）を使用
}

TEST_MODE_LOT = "adjustable"    # fixed なら常に1BTC固定 / adjustable なら可変ロット

volatility_term = 28       # 平均ボラティリティの計算に使う期間
stop_range = 4            # 何レンジ幅にストップを入れるか
trade_risk = 0.02          # 1トレードあたり口座の何％まで損失を許容するか
levarage = 10               # レバレッジ倍率の設定
start_funds = flag["records"]["funds"]     # シミュレーション時の初期資金

entry_times = 1            # 何回に分けて追加ポジションを取るか
entry_range = 1            # 何レンジごとに追加ポジションを取るか

stop_config = "OFF"   # ON / OFF / TRAILING の３つが設定可
stop_AF = 0.02             # 加速係数             # 加速係数
stop_AF_add = 0.03         # 加速係数を増やす度合
stop_AF_max = 0.3          # 加速係数の上限

wait = 0                   #  ループの待機時間
slippage = 0.000           #  手数料・スリッページ

long_t_vol_term = 240
short_t_vol_term = 10
vol_check_under = 0.4         #乖離率として計算する
vol_check_over = 1.0

logger.info("chart_sec = " + str(chart_sec))
logger.info("buy_term = " + str(buy_term))
logger.info("sell_term = " + str(sell_term))
logger.info("high_term = " + str(high_term))
logger.info("low_term = " + str(low_term))
logger.info("filter_VER = " + str(filter_VER))
logger.info("volatility_check_under = " + str(volatility_check_under))
logger.info("volatility_check_over = " + str(volatility_check_over))
logger.info("profit_filter_value = " + str(profit_filter_value))
logger.info("judge_price = " + str(judge_price))
logger.info("TEST_MODE_LOT = " + str(TEST_MODE_LOT))
logger.info("volatility_term = " + str(volatility_term))
logger.info("stop_range = " + str(stop_range))
logger.info("trade_risk = " + str(trade_risk))
logger.info("levarage = " + str(levarage))
logger.info("start_funds = " + str(start_funds))
logger.info("entry_times = " + str(entry_times))
logger.info("entry_range = " + str(entry_range))
logger.info("stop_config = " + str(stop_config))
logger.info("stop_AF = " + str(stop_AF))
logger.info("stop_AF_add = " + str(stop_AF_add))
logger.info("stop_AF_max = " + str(stop_AF_max))
logger.info("wait = " + str(wait))
logger.info("slippage = " + str(slippage))
logger.info("long_t_vol_term = " + str(long_t_vol_term))
logger.info("short_t_vol_term = " + str(short_t_vol_term))
logger.info("vol_check_under = " + str(vol_check_under))
logger.info("vol_check_over = " + str(vol_check_over))


#-------------補助ツールの関数--------------

first_time = True
processtime = time.time()
trail_time = time.time()

def get_price(min,i):
    e_cnt = 0

    while True:
        if e_cnt > 2:
            logger.info("3回連続で失敗したため、15分待機してやり直します")
            time.sleep(900)
            e_cnt = 0

        try:
            price = []
            params = {"periods" : min }
            response = requests.get("https://api.cryptowat.ch/markets/bitflyer/btcfxjpy/ohlc",params)
            data = response.json()

            if data["result"] is not None:
                for i in data["result"][str(min)]:
                    if i[1] != 0 and i[2] != 0 and i[3] != 0 and i[4] != 0:
                        price.append({ "close_time" : i[0],
                            "close_time_dt" : datetime.fromtimestamp(i[0]).strftime('%Y/%m/%d %H:%M'),
                            "open_price" : i[1],
                            "high_price" : i[2],
                            "low_price" : i[3],
                            "close_price": i[4],
                            "dekidaka": i[5]})
                return price
            else:
                logger.info("データが存在しません")
                return None

        except requests.exceptions.RequestException as e:
            e_cnt += 1
            logger.info("Cryptowatchの価格取得でエラー発生 : ")
            logger.info("60秒待機してやり直します")
            logger.info("エラー内容 : " , e)
            time.sleep(60)


# 時間と高値・安値をログに記録する関数
def log_price( data,flag ):
    log =  "時間： " + datetime.fromtimestamp(data["close_time"]).strftime('%Y/%m/%d %H:%M') + " 高値： " + str(data["high_price"]) + " 安値： " + str(data["low_price"]) + " 終値： " + str(data["close_price"])
    flag["records"]["log"].append(log)
    return flag


# 平均ボラティリティを計算する関数
def calculate_volatility( last_data ):

    high_sum = sum(i["high_price"] for i in last_data[-1 * volatility_term :])
    low_sum  = sum(i["low_price"]  for i in last_data[-1 * volatility_term :])
    volatility = round((high_sum - low_sum) / volatility_term)
    if time.time() - processtime > 3600:
        logger.info("現在の{0}期間の平均ボラティリティは{1}円です".format( volatility_term, volatility ))
    return volatility


# 単純移動平均を計算する関数
def calculate_MA( value,last_data,before=None ):
    if before is None:
        MA = sum(i["close_price"] for i in last_data[-1*value:]) / value
    else:
        MA = sum(i["close_price"] for i in last_data[-1*value + before: before]) / value
    return round(MA)


# 指数移動平均を計算する関数
def calculate_EMA( value,last_data ,before=None ):
    if before is not None:
        MA = sum(i["close_price"] for i in last_data[-2*value + before : -1*value + before]) / value
        EMA = (last_data[-1*value + before]["close_price"] * 2 / (value+1)) + (MA * (value-1) / (value+1))
        for i in range(value-1):
            EMA = (last_data[-1*value+before+1 + i]["close_price"] * 2 /(value+1)) + (EMA * (value-1) / (value+1))
    else:
        MA = sum(i["close_price"] for i in last_data[-2*value: -1*value]) / value
        EMA = (last_data[-1*value]["close_price"] * 2 / (value+1)) + (MA * (value-1) / (value+1))
        for i in range(value-1):
            EMA = (last_data[-1*value+1 + i]["close_price"] * 2 /(value+1)) + (EMA * (value-1) / (value+1))
    return round(EMA)

# 単純出来高移動平均を計算する関数
def calculate_VA( value,last_data,before=None ):
    if before is None:
        VA = sum(i["dekidaka"] for i in last_data[-1*value:]) / value
    else:
        VA = sum(i["dekidaka"] for i in last_data[-1*value + before: before]) / value
    return round(VA)

#-------------売買注文の関数-------------

def buy_order(back_test_mode):
    if back_test_mode == False:
        logger.info("成行買い注文実行のこーど")
#        print("成行買い注文実行の(バックテスト)")

#        order = bitflyer.create_order(
#            symbol = 'BTC/JPY',
#            type='market',
#            side='buy',
#            amount=lot,
#            params = { "product_code" : "FX_BTC_JPY" })

def sell_order(back_test_mode):
    if back_test_mode == False:
        logger.info("成行売り注文実行のこーど")
#        print("成行売り注文実行の(バックテスト)")
#        order = bitflyer.create_order(
#            symbol = 'BTC/JPY',
#            type='market',
#            side='sell',
#            amount=lot,
#            params = { "product_code" : "FX_BTC_JPY" })

filter_log_time = time.time()
#-------------エントリーフィルターの関数--------------

# エントリーフィルターの関数
def filter( signal , last_data, data ):

    global processtime
    global filter_log_time

    if filter_VER == "OFF":
        return True

    if filter_VER == "A":
        if len(last_data) < 200:
            return True
        if data["close_price"] > last_data[-200]["close_price"] and signal["side"] == "BUY":
            return True
        if data["close_price"] < last_data[-200]["close_price"] and signal["side"] == "SELL":
            return True

    if filter_VER == "B":
        if len(last_data) < 200:
            return True
        if data["close_price"] > calculate_MA(200) and signal["side"] == "BUY":
            return True
        if data["close_price"] < calculate_MA(200) and signal["side"] == "SELL":
            return True

    if filter_VER == "C":
        if len(last_data) < 20:
            return True
        if calculate_MA(20) > calculate_MA(20,-1) and signal["side"] == "BUY":
            return True
        if calculate_MA(20) < calculate_MA(20,-1) and signal["side"] == "SELL":
            return True

    if filter_VER == "D":
        if len(last_data) < 250:
            return True
        if calculate_EMA(200) < calculate_EMA(10) and signal["side"] == "BUY":
            return True
        if calculate_EMA(200) > calculate_EMA(10) and signal["side"] == "SELL":
            return True

    if filter_VER == "E":
        if len(last_data) < 700:
            return True
        if calculate_EMA(350) < calculate_EMA(25) and signal["side"] == "BUY":
            return True
        if calculate_EMA(350) > calculate_EMA(25) and signal["side"] == "SELL":
            return True

    if filter_VER == "F":
        if len(last_data) < 120:
            return True
        if calculate_MA(120) > calculate_MA(120,-1) and signal["side"] == "BUY":
            return True
        if calculate_MA(120) < calculate_MA(120,-1) and signal["side"] == "SELL":
            return True

    if filter_VER == "G":
        if len(last_data) < 180:
            return True
        if calculate_EMA(180,last_data) < calculate_EMA(90,last_data) and signal["side"] == "BUY":
            return True
        if calculate_EMA(180,last_data) > calculate_EMA(90,last_data) and signal["side"] == "SELL":
            return True
    try:
        volatility_highest = max(i["high_price"] for i in last_data[ (-1* volatility_check_term): ])
        volatility_lowest = min(i["low_price"] for i in last_data[ (-1* volatility_check_term): ])
    except Exception as ex:
        print(ex)
        print(last_data)
    term = max([high_term , low_term])

    volatility = (volatility_highest - volatility_lowest) / data["close_price"]

    dekidaka_kairiritu = 1
    last_short_va = 1
    last_long_va = 1
    if len(last_data) > max([long_t_vol_term, short_t_vol_term])-1:
        try:
            dekidaka_kairiritu = calculate_VA(long_t_vol_term, last_data) / calculate_VA(short_t_vol_term, last_data)
            last_short_va = calculate_VA(short_t_vol_term, last_data)
            last_long_va = calculate_VA(long_t_vol_term, last_data)
        except Exception:
            print("filter_error:出来高乖離率を0で計算します")
            dekidaka_kairiritu = last_long_va / last_short_va

    if filter_VER == "H":
        if len(last_data) < term:
            return True
        if dekidaka_kairiritu > vol_check_under and dekidaka_kairiritu < vol_check_over:
            if data["close_price"] > volatility_lowest and volatility > volatility_check_under and  volatility < volatility_check_over and signal["side"] == "BUY":
                return True
            if data["close_price"] < volatility_highest and volatility > volatility_check_under and volatility < volatility_check_over and signal["side"] == "SELL":
                return True
    return False


#-------------資金管理の関数--------------

# 注文ロットを計算する関数
def calculate_lot( last_data,data,flag ):

    # 固定ロットでのテスト時
    if TEST_MODE_LOT == "fixed":
        logger.info("固定ロット(1枚)でテスト中のため、1BTCを注文します")
        lot = 1
        volatility = calculate_volatility( last_data )
        stop = stop_range * volatility
        flag["position"]["ATR"] = round( volatility )
        return lot,stop,flag


    # 口座残高を取得する
    balance = flag["records"]["funds"]

    # 最初のエントリーの場合
    if flag["add-position"]["count"] == 0:

        # １回の注文単位（ロット数）と、追加ポジの基準レンジを計算する
        volatility = calculate_volatility( last_data )
        stop = stop_range * volatility
        calc_lot = np.floor( balance * trade_risk / stop * 100 ) / 100

        flag["add-position"]["unit-size"] = np.floor( calc_lot / entry_times * 100 ) / 100
        flag["add-position"]["unit-range"] = round( volatility * entry_range )
        flag["add-position"]["stop"] = stop
        flag["position"]["ATR"] = round( volatility )

        logger.info("現在のアカウント残高は{}円です".format( balance ))
        logger.info("許容リスクから購入できる枚数は最大{}BTCまでです".format( calc_lot ))
        logger.info("{0}回に分けて{1}BTCずつ注文します".format( entry_times, flag["add-position"]["unit-size"] ))

    # ２回目以降のエントリーの場合
    else:
        balance = round( balance - flag["position"]["price"] * flag["position"]["lot"] / levarage )

    # ストップ幅には、最初のエントリー時に計算したボラティリティを使う
    stop = flag["add-position"]["stop"]

    # 実際に購入可能な枚数を計算する
    able_lot = np.floor( balance * levarage / data["close_price"] * 100 ) / 100
    lot = min(able_lot, flag["add-position"]["unit-size"])
    logger.info("証拠金から購入できる枚数は最大{}BTCまでです".format( able_lot ))

    return lot,stop,flag

# 複数回に分けて追加ポジションを取る関数
def add_position( data,last_data,flag ):

    # ポジションがない場合は何もしない
    if flag["position"]["exist"] == False:
        return flag

    # 固定ロット（1BTC）でのテスト時は何もしない
    if TEST_MODE_LOT == "fixed":
        return flag

    # 最初（１回目）のエントリー価格を記録
    if flag["add-position"]["count"] == 0:
        flag["add-position"]["first-entry-price"] = flag["position"]["price"]
        flag["add-position"]["last-entry-price"] = flag["position"]["price"]
        flag["add-position"]["count"] += 1

    while True:

        # 以下の場合は、追加ポジションを取らない
        if flag["add-position"]["count"] >= entry_times:
            return flag

        # この関数の中で使う変数を用意
        first_entry_price = flag["add-position"]["first-entry-price"]
        last_entry_price = flag["add-position"]["last-entry-price"]
        unit_range = flag["add-position"]["unit-range"]
        current_price = data["close_price"]


        # 価格がエントリー方向に基準レンジ分だけ進んだか判定する
        should_add_position = False
        if flag["position"]["side"] == "BUY" and (current_price - last_entry_price) > unit_range:
            should_add_position = True
        elif flag["position"]["side"] == "SELL" and (last_entry_price - current_price) > unit_range:
            should_add_position = True
        else:
            break

        # 基準レンジ分進んでいれば追加注文を出す
        if should_add_position == True:
            logger.info("前回のエントリー価格{0}円からブレイクアウトの方向に{1}ATR（{2}円）以上動きました".format( last_entry_price, entry_range, round( unit_range ) ))
            logger.info("{0}/{1}回目の追加注文を出します".format(flag["add-position"]["count"] + 1, entry_times))

            # 注文サイズを計算
            lot,stop,flag = calculate_lot( last_data,data,flag )
            if lot < 0.01:
                logger.info("注文可能枚数{}が、最低注文単位に満たなかったので注文を見送ります".format(lot))
                flag["add-position"]["count"] += 1
                return flag

            # 追加注文を出す
            if flag["position"]["side"] == "BUY":
                entry_price = first_entry_price + (flag["add-position"]["count"] * unit_range)
                #entry_price = round((1 + slippage) * entry_price)

                logger.info("現在のポジションに追加して、{0}円で{1}BTCの買い注文を出します".format(entry_price,lot))

                buy_order(backtest_flag)

            if flag["position"]["side"] == "SELL":
                entry_price = first_entry_price - (flag["add-position"]["count"] * unit_range)
                #entry_price = round((1 - slippage) * entry_price)

                logger.info("現在のポジションに追加して、{0}円で{1}BTCの売り注文を出します".format(entry_price,lot))
                sell_order(backtest_flag)

            # ポジション全体の情報を更新する
            flag["position"]["stop"] = stop
            flag["position"]["price"] = int(round(( flag["position"]["price"] * flag["position"]["lot"] + entry_price * lot ) / ( flag["position"]["lot"] + lot )))
            flag["position"]["lot"] = np.round( (flag["position"]["lot"] + lot) * 100 ) / 100

            if flag["position"]["side"] == "BUY":
                logger.info("{0}円の位置にストップを更新します".format(flag["position"]["price"] - stop))
            elif flag["position"]["side"] == "SELL":
                logger.info("{0}円の位置にストップを更新します".format(flag["position"]["price"] + stop))
            logger.info("現在のポジションの取得単価は{}円です".format(flag["position"]["price"]))
            logger.info("現在のポジションサイズは{}BTCです".format(flag["position"]["lot"]))

            flag["add-position"]["count"] += 1
            flag["add-position"]["last-entry-price"] = entry_price

    return flag


# トレイリングストップの関数
def trail_stop( data,flag ):

    # まだ追加ポジションの取得中であれば何もしない
    if flag["add-position"]["count"] < entry_times and TEST_MODE_LOT != "fixed":
        return flag

    # 高値／安値がエントリー価格からいくら離れたか計算
    if flag["position"]["side"] == "BUY":
        moved_range = round( data["high_price"] - flag["position"]["price"] )
    if flag["position"]["side"] == "SELL":
        moved_range = round( flag["position"]["price"] - data["low_price"] )

    # 最高値・最安値を更新したか調べる

    if moved_range < 0 or flag["position"]["stop-EP"] >= moved_range:
        return flag
    else:
        flag["position"]["stop-EP"] = moved_range

    global trail_time

    if stop_config == "TRAILING":
        if time.time() - trail_time > chart_sec:
            trail_time = time.time()
            # 加速係数に応じて損切りラインを動かす
            flag["position"]["stop"] = round(flag["position"]["stop"] - (
                moved_range + flag["position"]["stop"] ) * flag["position"]["stop-AF"])
            flag["position"]["stop-AF"] = round( flag["position"]["stop-AF"] + stop_AF_add ,2 )
            if flag["position"]["stop-AF"] >= stop_AF_max:
                flag["position"]["stop-AF"] = stop_AF_max
            # ログ出力
            if flag["position"]["side"] == "BUY":
                logger.info("トレイリングストップの発動：ストップ位置を{}円に動かして、加速係数を{}に更新します".format(
                    round(flag["position"]["price"] - flag["position"]["stop"]) , flag["position"]["stop-AF"] ))
            else:
                logger.info("トレイリングストップの発動：ストップ位置を{}円に動かして、加速係数を{}に更新します".format(
                    round(flag["position"]["price"] + flag["position"]["stop"]) , flag["position"]["stop-AF"] ))

    return flag

#-------------売買ロジックの部分の関数--------------

# ドンチャンブレイクを判定する関数
def donchian( data,last_data ):
    highest = max(i["high_price"] for i in last_data[ (-1* buy_term): ])
    lowest = min(i["low_price"] for i in last_data[ (-1* sell_term): ])

    if data[ judge_price["BUY"] ] > highest:
        return {"side":"SELL","price":lowest ,"highest":highest , "lowest":lowest}

    if data[ judge_price["SELL"] ] < lowest:
        return {"side":"BUY","price":highest,"highest":highest , "lowest":lowest}

    return {"side" : None , "price":0 ,"highest":highest , "lowest":lowest}

# エントリー注文を出す関数
def entry_signal(data, last_data, flag, price):
    signal = donchian( data,last_data )
    #flag = order_estrangement_check( data,last_data,flag,signal )
    #volatility = calculate_volatility( last_data )
    if signal["side"] == "BUY":
        # フィルター条件を確認
        if filter( signal , last_data, data ) == False:
            #logger.info("フィルターのエントリー条件を満たさなかったため、エントリーしません")
            return flag

        logger.info("BUYエントリーシグナル：過去{0}足の最安値{1}円を、判定時の価格{2}円でブレイクしました".format(buy_term,signal["price"],data[judge_price["SELL"]]))
        lot,stop,flag = calculate_lot( last_data,data,flag )

        if lot > 0.01:
            logger.info("{0}円で{1}BTCの買い注文を出します".format(data["close_price"],lot))

            buy_order(backtest_flag)

            logger.info("{0}円にストップを入れます".format(data["close_price"] - stop))
            flag["position"]["lot"],flag["position"]["stop"] = lot,stop
            flag["position"]["exist"] = True
            flag["position"]["side"] = "BUY"
            flag["position"]["price"] = data["close_price"]

            now_price_logging(data,last_data,price,True,flag)

        else:
            logger.info("注文可能枚数{}が、最低注文単位に満たなかったので注文を見送ります".format(lot))

    if signal["side"] == "SELL":
        # フィルター条件を確認
        if filter( signal , last_data, data ) == False:
            #logger.info("フィルターのエントリー条件を満たさなかったため、エントリーしません")
            return flag

        logger.info("SELLエントリーシグナル：過去{0}足の最高値{1}円を、判定時の価格{2}円でブレイクしました".format(sell_term,signal["price"],data[judge_price["BUY"]]))
        lot,stop,flag = calculate_lot( last_data,data,flag )
        # エントリー前に現在価格が判定価格からどれくらい離れているかチェック
        #oec_price = data["close_price"] - flag["position"]["oec"]
        #print(oec_price)

        #if data["low_price"] < oec_price:
        #    logger.info("{0}円のオーダー前の乖離ラインに引っかかりました。".format( oec_price ))
        #    return flag

        if lot > 0.01:
            logger.info("{0}円で{1}BTCの売り注文を出します".format(data["close_price"],lot))

            # 売り注文のコードを入れる
            sell_order(backtest_flag)

            logger.info("{0}円にストップを入れます".format(data["close_price"] + stop))
            flag["position"]["lot"],flag["position"]["stop"] = lot,stop
            flag["position"]["exist"] = True
            flag["position"]["side"] = "SELL"
            flag["position"]["price"] = data["close_price"]

            now_price_logging(data,last_data,price,True,flag)

        else:
            logger.info("注文可能枚数{}が、最低注文単位に満たなかったので注文を見送ります".format(lot))

    return flag


profit_print_time = 0

# 手仕舞いのシグナルが出たら決済の成行注文 + ドテン注文 を出す関数
def close_position( data,last_data,flag ,price):
    global profit_print_time

    if flag["position"]["exist"] == False:
        return flag

    flag["position"]["count"] += 1
    signal = donchian( data,last_data )

    if flag["position"]["side"] == "BUY":
        if signal["side"] == "SELL": #and pre_profit > で収支がプラスの時だけクローズする。
            logger.info("買いクローズ：過去{0}本足の最高値{1}円を、直近の価格が{2}円でブレイクしました".format(buy_term,signal["price"],data[judge_price["BUY"]]))
            logger.info(str(data["close_price"]) + "円あたりで成行注文を出してポジションを決済します")

            lot,stop,flag = calculate_lot( last_data,data,flag )

            sell_order(backtest_flag)

            records( flag,data,data["close_price"] )
            flag["position"]["side"] = ""
            flag["position"]["price"] = 0
            flag["position"]["stop"] = 0
            flag["position"]["lot"] = 0
            flag["position"]["exist"] = False
            flag["position"]["count"] = 0
            flag["position"]["stop-AF"] = stop_AF
            flag["position"]["stop-EP"] = 0
            flag["add-position"]["count"] = 0

            # ドテン注文の箇所
            if filter( signal , last_data, data ) == False:
                logger.info("フィルターのエントリー条件を満たさなかったため、ドテンエントリーはしません")
                now_price_logging(data,last_data,price,True,flag)
                return flag

            if lot > 0.01:
                logger.info("{0}円で{1}BTCの売りの注文を入れてドテンします".format(data["close_price"],lot))

                sell_order(backtest_flag)

                logger.info("{0}円にストップを入れます".format(data["close_price"] + stop))
                flag["position"]["lot"],flag["position"]["stop"] = lot,stop
                flag["position"]["exist"] = True
                flag["position"]["side"] = "SELL"
                flag["position"]["price"] = data["close_price"]
                now_price_logging(data,last_data,price,True,flag)
                return flag


    if flag["position"]["side"] == "SELL":
        if signal["side"] == "BUY":
            logger.info("売りクローズ：過去{0}本足の最安値{1}円を、直近の価格が{2}円でブレイクしました".format(sell_term,signal["price"],data[judge_price["SELL"]]))
            logger.info(str(data["close_price"]) + "円あたりで成行注文を出してポジションを決済します")

            lot,stop,flag = calculate_lot( last_data,data,flag )

            buy_order(backtest_flag)

            records( flag,data,data["close_price"] )
            flag["position"]["side"] = ""
            flag["position"]["price"] = 0
            flag["position"]["stop"] = 0
            flag["position"]["lot"] = 0
            flag["position"]["exist"] = False
            flag["position"]["count"] = 0
            flag["position"]["stop-AF"] = stop_AF
            flag["position"]["stop-EP"] = 0
            flag["add-position"]["count"] = 0

            # ドテン注文の箇所
            if filter( signal , last_data, data) == False:
                logger.info("フィルターのエントリー条件を満たさなかったため、ドテンエントリーはしません")
                now_price_logging(data,last_data,price,True,flag)
                return flag

            if lot > 0.01:
                logger.info("{0}円で{1}BTCの買いの注文を入れてドテンします".format(data["close_price"],lot))

                buy_order(backtest_flag)

                logger.info("{0}円にストップを入れます".format(data["close_price"] - stop))
                flag["position"]["lot"],flag["position"]["stop"] = lot,stop
                flag["position"]["exist"] = True
                flag["position"]["side"] = "BUY"
                flag["position"]["price"] = data["close_price"]
                now_price_logging(data,last_data,price,True,flag)
                return flag
    return flag


# 損切ラインにかかったら成行注文で決済する関数
def stop_position(  data,last_data,flag,price ):
    global first_time

    # トレイリングストップを実行
    flag = trail_stop( data,flag )

    if flag["position"]["side"] == "BUY":
        stop_price = flag["position"]["price"] - flag["position"]["stop"]
        if data["low_price"] < stop_price:
            logger.info("{0}円の損切ラインに引っかかりました。".format( stop_price ))
            stop_price = round( stop_price - 2 * calculate_volatility(last_data) / ( chart_sec / 60) )
            logger.info(str(stop_price) + "円あたりで成行注文を出してポジションを決済します")

            sell_order(backtest_flag)
            first_time = True

            # ポジションを解消した後、ポジションを持っていないフラグとなる。
            records( flag,data,stop_price,"STOP" )
            flag["position"]["side"] = ""
            flag["position"]["price"] = 0
            flag["position"]["stop"] = 0
            flag["position"]["lot"] = 0
            flag["position"]["exist"] = False
            flag["position"]["count"] = 0
            flag["position"]["stop-AF"] = stop_AF
            flag["position"]["stop-EP"] = 0
            flag["add-position"]["count"] = 0


    if flag["position"]["side"] == "SELL":
        stop_price = flag["position"]["price"] + flag["position"]["stop"]
        if data["high_price"] > stop_price:
            logger.info("{0}円の損切ラインに引っかかりました。".format( stop_price ))
            stop_price = round( stop_price + 2 * calculate_volatility(last_data) / (chart_sec / 60) )
            logger.info(str(stop_price) + "円あたりで成行注文を出してポジションを決済します")

            buy_order(backtest_flag)
            first_time = True

            # ポジションを解消した後、ポジションを持っていないフラグとなる。
            records( flag,data,stop_price,"STOP" )
            flag["position"]["side"] = ""
            flag["position"]["price"] = 0
            flag["position"]["stop"] = 0
            flag["position"]["lot"] = 0
            flag["position"]["exist"] = False
            flag["position"]["count"] = 0
            flag["position"]["stop-AF"] = stop_AF
            flag["position"]["stop-EP"] = 0
            flag["add-position"]["count"] = 0

    now_price_logging(data, last_data, price, False, flag)
    return flag

def pre_records(flag, data, close_price, close_type=None):

    entry_price = int(round(flag["position"]["price"] * flag["position"]["lot"]))
    exit_price = int(round(close_price * flag["position"]["lot"]))
    trade_cost = round( exit_price * slippage )

    buy_profit = exit_price - entry_price - trade_cost
    sell_profit = entry_price - exit_price - trade_cost
    pre_profit = 0

    if flag["position"]["side"] == "BUY":
        pre_profit = buy_profit

    if flag["position"]["side"] == "SELL":
        pre_profit = sell_profit

    return pre_profit

def records(flag,data,close_price,close_type=None):

    # 取引手数料等の計算
    entry_price = int(round(flag["position"]["price"] * flag["position"]["lot"]))
    exit_price = int(round(close_price * flag["position"]["lot"]))
    trade_cost = round( exit_price * slippage )

    log = "スリッページ・手数料として " + str(trade_cost) + "円を考慮します"
    logger.info(log)
    flag["records"]["slippage"].append(trade_cost)

    # 手仕舞った日時と保有期間を記録
    flag["records"]["date"].append(data["close_time_dt"])
    flag["records"]["holding-periods"].append( flag["position"]["count"] )

    # 損切りにかかった回数をカウント
    if close_type == "STOP":
        flag["records"]["stop-count"].append(1)
    else:
        flag["records"]["stop-count"].append(0)

    # 値幅の計算
    buy_profit = exit_price - entry_price - trade_cost
    sell_profit = entry_price - exit_price - trade_cost

    # 利益が出てるかの計算
    if flag["position"]["side"] == "BUY":
        flag["records"]["side"].append( "BUY" )
        flag["records"]["profit"].append( buy_profit )
        flag["records"]["return"].append( round( buy_profit / entry_price * 100, 4 ))
        flag["records"]["funds"] = flag["records"]["funds"] + buy_profit
        flag["records"]["last_profit"] = buy_profit
        if buy_profit  > 0:
            log = str(buy_profit) + "円の利益です"
            logger.info(log)
        else:
            log = str(buy_profit) + "円の損失です"
            logger.info(log)

    if flag["position"]["side"] == "SELL":
        flag["records"]["side"].append( "SELL" )
        flag["records"]["profit"].append( sell_profit )
        flag["records"]["return"].append( round( sell_profit / entry_price * 100, 4 ))
        flag["records"]["funds"] = flag["records"]["funds"] + sell_profit
        flag["records"]["last_profit"] = sell_profit
        if sell_profit > 0:
            log = str(sell_profit) + "円の利益です"
            logger.info(log)
        else:
            log = str(sell_profit) + "円の損失です"
            logger.info(log)

    return flag


def now_price_logging(data, last_data, price, log_flag, flag):
    global processtime
    global first_time

    if log_flag:
        first_time = True

    signal = donchian( data, last_data )

    if time.time() - processtime > 3600 or first_time:
        first_time = False
        processtime = time.time()
        for i in range(5,0,-1):
            print(last_data[-i])
        logger.info('chart_sec：' +  str(chart_sec))
        logger.info('シミュ上の証拠金：' + '￥' + '{:,}'.format(flag["records"]["funds"]))
        logger.info('チャネル内の高値：' + '￥' + '{:,}'.format(signal["highest"]))
        logger.info('現在足の価格　　：' + '￥' + '{:,}'.format(price[-1]["close_price"]))
        logger.info('チャネル内の安値：' + '￥' + '{:,}'.format(signal["lowest"]))
        logger.info('ポジション　　　：' + '￥' + '{:,}'.format(flag["position"]["price"]) + ' ' + flag["position"]["side"] )
        logger.info('ポジションサイズ：' + str(flag["position"]["lot"]))

#------------ここからメイン処理--------------

# 価格チャートを取得
need_term = max(buy_term,sell_term, volatility_term, long_t_vol_term)

# flag = {
#     "position":{
#         "exist" : False,
#         "side" : "",
#         "price": 0,
#         "stop":0,
#         "stop-AF": stop_AF,
#         "stop-EP":0,
#         "ATR":0,
#         "lot":0,
#         "count":0,
#         "oec":0,
#         "oec-AF": stop_AF,
#         "oec-EP":0
#     },
#     "add-position":{
#         "count":0,
#         "first-entry-price":0,
#         "last-entry-price":0,
#         "unit-range":0,
#         "unit-size":0,
#         "stop":0
#     },
#     "records":{
#         "date":[],
#         "profit":[],
#         "return":[],
#         "side":[],
#         "stop-count":[],
#         "funds" : start_funds,
#         "holding-periods":[],
#         "slippage":[],
#         "log":[]
#     }
# }

def main_loop():
    fh = None
    skiptime = time.time()
    skipflag = True
    while True:
    # try:
        if fh != None:
            logger.removeHandler(fh)
        fh = logging.FileHandler(os.path.splitext(os.path.basename(__file__))[0] + format(datetime.now().strftime("-%Y-%m-%d")) + '.log')
        fh.setLevel(logging.DEBUG)
        logger.addHandler(fh)
        fh.setFormatter(formatter)

        with open(JSON_FILE_PATH, mode = 'r', encoding = 'utf-8') as jr:
            json_txt = jr.read()
            json_txt = str(json_txt).replace("'", '"').replace('True', 'true').replace('False', 'false')
            json_data = json.loads(json_txt)

        flag = json_data
        flag["records"]["log"] = []

        last_data = []
        price = get_price(chart_sec,need_term+1)
        del price[-1]
        #print(price)

        i = 0
        #priceは最新足が後ろの方に入っている
        # ドンチャンの判定に使う期間分の安値・高値データを準備する
        while True:
            if len(last_data) < need_term:
                last_data.append(price[-1+i+(len(price)-need_term)])
                flag = log_price(price[-1+i+(len(price)-need_term)],flag)
                i += 1
                continue
            break

        data = price[-1]
        flag = log_price(data,flag)
        #print(last_data)    #price[-1+i+(len(price)-need_term)] で1本足前からneed_term分の足を格納していることを確認
        #print(data) #price[-1]　最新足であることを確認。backtestでは、確定した足によって判別していた。

        if flag["records"]["last_profit"] < 0:
            if skipflag:
                skipflag = False
                skiptime = time.time()
                time.sleep(2)
                continue
            if time.time() - skiptime < 180:
                time.sleep(2)
                continue
            else:
                flag["records"]["last_profit"] = 0
        else:
            skipflag = True

        # ポジションがある場合
        if flag["position"]["exist"]:
            if stop_config != "OFF":
                flag = stop_position( data,last_data,flag,price )
            flag = close_position( data,last_data,flag,price )
            flag = add_position( data,last_data,flag )
        else: # ポジションがない場合
            flag = entry_signal( data,last_data,flag,price )

        now_price_logging(data,last_data,price,False,flag)

        #フォワードテスト用
        with open(os.path.splitext(os.path.basename(__file__))[0] + 'flag.json','w') as fw:
            text = json.dumps(flag)
            fw.write(text)

        gc.collect()
        time.sleep(1)

    # except Exception as ex:
    #     logger.error(ex)
    #     time.sleep(120)

if __name__ == '__main__':
    main_loop()

   # TODO：実際の取引の際に必要なこと
   # ・ポジションと証拠金などアカウントの情報を取り込む関数
   # ・オーダーを出した後、ポジションチェックでポジションを確認して、ずれをレポートする
   # 　->ずれの大きさによっては、ポジションをキャンセル。修正は成行の場合は無理
   # ・成行注文ではなくて指値を入れるようにする
   # 　->これは指値注文にしてパフォーマンスが向上するかどうかテストしてから。よくなるはずだがロジック的に逆の結果になる可能性もある
   # ・オーダーが通ったかチェックする関数の作成
   # 　１．エントリーやクローズの条件にかかったらフラグを保存。オーダーを通した後にオーダーが通ったか監視する
   # 　２．
