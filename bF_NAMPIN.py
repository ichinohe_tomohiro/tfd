#!/usr/bin/env python
# -*- coding: utf-8 -*-
#bFNAMPIN_Ver1.0

import json
import logging
import numpy as np
import os
import pybitflyer
import requests
import talib as ta
import threading
import time
import websocket
from datetime import datetime
from os import path

#SETTING------------------------------------------------------------------------------
API = 'CQ8viyxQpnEEC6LQwuoVqj'
SECRET = 'PEa8PCd2wv0sDtzNyJQbeC/t6Cvcdnm2OMq5goi2CHs='

LOT           = 0.01    #初期ロット
LOT_MAG       = 1.2     #ロット倍率
ENTRY_TIMES   = 20      #ナンピン回数
SPREAD_FILTER = 20      #スプレッドフィルター この数値以上スプレッドが開いているときにはエントリーしない

TAKE_PROFIT   = 200     #利確値幅(単位：円)

POS_SLEEP   = 1.5       #ポジションチェックループスリープ(秒)

symbol = 'FX_BTC_JPY'   #シンボル設定
log_p = ''              #logファイル作成場所

#SETTING------------------------------------------------------------------------------

bitFlyer = pybitflyer.API(api_key = API , api_secret = SECRET)
bitFlyer.api_url = 'https://api.bitflyer.com'

def logset(logger,name,log_p):
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(log_p + name + '_{0:%m-%d-%H-%M}'.format(datetime.today())+'.log')
    sh = logging.StreamHandler()
    logger.addHandler(fh)
    logger.addHandler(sh)
    fh.setFormatter(logging.Formatter('[%(asctime)s|%(levelname)7s|LINE:%(lineno)3d] %(message)s'))
    sh.setFormatter(logging.Formatter('[%(asctime)s|%(levelname)7s|LINE:%(lineno)3d] %(message)s'))

def limit_order(size,price,side,expire = 10000):
    try:
        limitorder = bitFlyer.sendchildorder(product_code = symbol,
                                 child_order_type = "LIMIT",
                                 side = side,
                                 price = price,
                                 size = abs(size),
                                 minute_to_expire = expire,
                                 time_in_force = "GTC"
                                 )
        return limitorder['child_order_acceptance_id']
    except Exception as e:
        logger.exception(e)
        return None

def market_order(size,side):
    try:
        marketorder = bitFlyer.sendchildorder(product_code=symbol,
                                 child_order_type="MARKET",
                                 side = side,
                                 size = abs(size),
                                 minute_to_expire = 1,
                                 time_in_force = "GTC"
                                 )
        return marketorder['child_order_acceptance_id']
    except Exception as e:
        logger.exception(e)
        return None

def cancelAllOrder():
    try:
        cancel = bitFlyer.cancelallchildorders(product_code =symbol)
        return cancel
    except Exception as e:
        logger.exception(e)
        return None

def getchildorders():
    try:
        orders = bitFlyer.getchildorders(product_code =symbol,child_order_state = "ACTIVE")
        return orders
    except Exception as e:
        logger.exception(e)
        return None

def cryptoOHLCV(period,asc=False):
    res = requests.get('https://api.cryptowat.ch/markets/bitflyer/btcfxjpy/ohlc?periods=60*period')
    ohlcv = res.json()
    if asc == False:
        ohlcvT = list(map(list, zip(*ohlcv['result'][str(60*period)][::-1])))
    else:
        ohlcvT = list(map(list, zip(*ohlcv['result'][str(60*period)])))
    open   = np.array(ohlcvT[1], dtype=np.float)
    high   = np.array(ohlcvT[2], dtype=np.float)
    low    = np.array(ohlcvT[3], dtype=np.float)
    close  = np.array(ohlcvT[4], dtype=np.float)
    volume = np.array(ohlcvT[5], dtype=np.float)
    return { 'o' : open , 'h' : high , 'l' : low , 'c' : close , 'v' : volume }

def atr():
    OHLCV = cryptoOHLCV(1440,asc=True)
    high  = OHLCV['h']
    low   = OHLCV['l']
    close = OHLCV['c']
    ATR = ta.ATR(high, low, close, timeperiod=14)[-1]
    return ATR

# def volatility():
#     OHLCV = cryptoOHLCV(60,asc=False)
#     high  = OHLCV['h']
#     low   = OHLCV['l']
#     close = OHLCV['c']
#     volatility = (high - low) / close
#     return volatility

class main:

    def  __init__(self):
        self.connect()
        self.channels = ['lightning_executions_'+symbol,'lightning_ticker_'+symbol]
        self.ticker = None
        self.ask = 0
        self.bid = 0
        self.spread = 0
        self.ltp = 0
        self.size = 0
        self.ave_price = 0
        self.pnl = 0
        self.TP_price = None
        self.ORDER_LOT = []
        self.exec_data = []

    def connect(self):
        self.ws = websocket.WebSocketApp(
            'wss://ws.lightstream.bitflyer.com/json-rpc', header=None,
            on_open = self.on_open, on_message = self.on_message,
            on_error = self.on_error, on_close = self.on_close)
        self.ws.keep_running = True
        self.thread = threading.Thread(target=lambda: self.ws.run_forever())
        self.thread.daemon = True
        self.thread.start()

    def is_connected(self):
        return self.ws.sock and self.ws.sock.connected

    def disconnect(self):
        self.ws.keep_running = False
        self.ws.close()

    def on_message(self, ws, message):
        params = json.loads(message)['params']
        recept_channel = params['channel']
        recept_data = params['message']
        if recept_channel == 'lightning_ticker_'+symbol:
            self.ticker = recept_data
            self.ltp = self.ticker["ltp"]
            self.bid = self.ticker["best_bid"]
            self.ask = self.ticker["best_ask"]
            self.spread = self.ask - self. bid
        elif recept_channel == 'lightning_executions_'+symbol:
            self.exec_data.extend(recept_data)
            if len(self.exec_data) > 1000:
                del self.exec_data[0:len(self.exec_data)-1000-1]

    def on_error(self, ws, error):
        self.disconnect()
        time.sleep(0.5)
        self.connect()

    def on_close(self, ws):
        logger.info('Websocket disconnected')

    def on_open(self, ws):
        for channel in self.channels:
            output_json = json.dumps({'method' : 'subscribe','params' : {'channel' : channel}})
            ws.send(output_json)
        logger.info('Websocket connected')

    def entry_logic(self):

        self.entry_signal = None
        # vol = volatility()
        S_CLOSE = cryptoOHLCV(period = 60 , asc = True)['c']
        L_CLOSE = cryptoOHLCV(period = 1440 , asc = True)['c']
        S_EMA20 = int(ta.EMA(S_CLOSE,timeperiod = 20)[-1])
        S_EMA75 = int(ta.EMA(S_CLOSE,timeperiod = 75)[-1])
        L_EMA20 = int(ta.EMA(L_CLOSE,timeperiod = 20)[-1])
        L_EMA75 = int(ta.EMA(L_CLOSE,timeperiod = 75)[-1])
        if S_EMA20 < S_EMA75 and L_EMA20 < L_EMA75:
            self.entry_signal = 'SHORT'
        elif S_EMA20 > S_EMA75 and L_EMA20 > L_EMA75:
            self.entry_signal = 'LONG'
        else:
            self.entry_signal = None

    def lot_calcu(self):

        self.ORDER_LOT = []
        self.ORDER_LOT.append(LOT)
        n_lot = LOT
        for i in range(ENTRY_TIMES):
            lot = round(n_lot*LOT_MAG,3)
            self.ORDER_LOT.append(lot)
            n_lot = lot

    def order_range(self):
        ATR = atr()/3
        self.ORDER_RANGE = int(ATR/ENTRY_TIMES)

    def exec_conf(self,ID,side):
        entry_price = None
        entrytime = nowtime = int(time.time())
        while entry_price == None:
            for i in range(len(self.exec_data)-1):
                if self.exec_data[i][str(side)+'_child_order_acceptance_id'] == ID:
                    entry_price = self.exec_data[i]['price']
                    break
            time.sleep(0.5)
            nowtime = int(time.time())
            if nowtime - entrytime > 60:
                break
        return entry_price

    def first_entry(self):
        if self.entry_signal == 'LONG' and self.spread < SPREAD_FILTER:
            order = market_order(self.NEXT_LOT, side = 'SELL')
            time.sleep(0.5)
            entry_price = self.exec_conf(order,side = 'buy')
            logger.info('BUY First order at @' + str(entry_price))
            del self.ORDER_LOT[0]
            time.sleep(1)
            self.nampin_mode('LONG')
        elif self.entry_signal == 'SHORT'and self.spread < SPREAD_FILTER:
            order = market_order(self.NEXT_LOT, side = 'SELL')
            time.sleep(0.5)
            entry_price = self.exec_conf(order,side = 'sell')
            logger.info('SELL First order at @' + str(entry_price))
            del self.ORDER_LOT[0]
            time.sleep(1)
            self.nampin_mode('SHORT')

    def nampin_mode(self,side):
        logger.info('nampin_mode start')
        while not -0.01 < self.size < 0.01:
            try:
                self.close_order(side)
                self.nampin_order(side)
                time.sleep(5)
            except Exception as e:
                logger.exception(e)
        cancelAllOrder()
        self.lot_calcu()
        self.order_range()
        self.TP_price = None
        logger.info('nampin_mode is finished')

    def nampin_order(self,side):
        ORDER_side = 'BUY' if side == 'LONG' else 'SELL'
        if ORDER_side == 'BUY':
            if self.last_exec_price > self.ltp + self.ORDER_RANGE and self.spread < SPREAD_FILTER:
                order = market_order(self.NEXT_LOT, side = 'BUY')
                time.sleep(0.5)
                entry_price = self.exec_conf(order,side = 'buy')
                logger.info('BUY Nampin order at @' + str(entry_price))
                del self.ORDER_LOT[0]
                time.sleep(1)
        else:
            if self.last_exec_price < self.ltp - self.ORDER_RANGE and self.spread < SPREAD_FILTER:
                order = market_order(self.NEXT_LOT, side = 'SELL')
                time.sleep(0.5)
                entry_price = self.exec_conf(order,side = 'sell')
                logger.info('SELL Nampin order at @' + str(entry_price))
                del self.ORDER_LOT[0]
                time.sleep(1)

    def close_order(self,side):
        TP_side = 'SELL' if side == 'LONG' else 'BUY'
        orders = getchildorders()
        if len(orders) > 1:
            cancelAllOrder()
            logger.info('order all cancel at many orders')
            time.sleep(5)
        elif len(orders) == 0:
            self.TP_price = self.ave_price + TAKE_PROFIT if side == 'LONG' else self.ave_price - TAKE_PROFIT
            limit_order(self.size,self.TP_price,side = TP_side)
            logger.info('position close order at @' + str(self.TP_price))
            time.sleep(5)
        else:
            self.TP_price = self.ave_price + TAKE_PROFIT if side == 'LONG' else self.ave_price - TAKE_PROFIT
            if not orders[0]['price'] == self.TP_price:
                cancelAllOrder()
                logger.info('order all cancel at TP_price diff')
                time.sleep(5)
            elif not orders[0]['size'] == abs(self.size):
                cancelAllOrder()
                logger.info('order all cancel at size diff')
                time.sleep(5)

    def mainroop(self):
        while True:
            try:
                self.entry_logic()
                if -0.01 < self.size < 0.01:
                    if not self.entry_signal == None:
                        cancelAllOrder()
                        self.lot_calcu()
                        self.order_range()
                        self.first_entry()
                else:
                    if self.size >= 0.01:
                        self.nampin_mode('LONG')
                    else:
                        self.nampin_mode('SHORT')
                time.sleep(5)
#                status,health = getboardstate()
            except Exception as e:
                logger.exception(e)

    def pos_roop(self):
        while True:
            try:
                poss = bitFlyer.getpositions(product_code=symbol)
                print(poss)
                if len(poss) > 0:
                    self.last_exec_price = int(poss[-1]['price'])
                else:
                    self.last_exec_price = None
                size= pnl = price = 0
                for p in poss:
                    if p['side'] == 'BUY':
                        size += p['size']
                        price += p['price']*p['size']
                        pnl += p['pnl']
                    if p['side'] == 'SELL':
                        size -= p['size']
                        price += p['price']*p['size']
                        pnl += p['pnl']
                if size == 0:
                    price = 0
                else:
                    price = abs(price/size)
                self.size = round(size,8)
                self.ave_price = round(price)
                self.pnl = pnl
                self.NEXT_LOT = self.ORDER_LOT[0]
                print('')
                print('[POSITION]  | SIZE : {:>11.8f} | AVE_PRICE : {:>7.1f} | PNL : {:7.2f} |'\
                .format(self.size,self.ave_price,self.pnl))
                print('[MARKET]    | LTP : {:>6} | BEST_ASK : {:>6} | BEST_BID : {:>6} | SPREAD : {:>3} |\
                '.format(self.ltp,self.ask,self.bid,self.spread))
                print('[ORDER]     | LAST_EXEC_PRICE : {} | NEXT_LOT : {} | REMAINING_ORDER : {} | ORDER_RANGE : {} | TP_PRICE : {} |\
                '.format(self.last_exec_price,self.NEXT_LOT,len(self.ORDER_LOT),self.ORDER_RANGE,self.TP_price))
                time.sleep(POS_SLEEP)
            except Exception as e:
                logger.exception("GET POSITION ERROR:" + str(e))
                time.sleep(1)

    def start(self):
        self.lot_calcu()
        logger.info(self.ORDER_LOT)
        self.order_range()
        thread_1 = threading.Thread(target=self.mainroop,name='mainroop')
        thread_2 = threading.Thread(target=self.pos_roop,name='pos_roop')
        thread_1.start()
        thread_2.start()
        while True:
            time.sleep(1)

if __name__ == '__main__':
    filename = os.path.splitext(path.basename(__file__))[0]
    logger = logging.getLogger(__name__)
    logset(logger,filename,log_p)
    th = main()
    th.start()
