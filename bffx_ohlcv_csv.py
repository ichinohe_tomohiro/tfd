# coding: utf-8
import threading
from collections import deque
import websocket
import warnings
import time
import json
import pandas
import dateutil.parser
from datetime import datetime,timedelta
from logging import getLogger, StreamHandler, INFO
logger = getLogger(__name__)
handler = StreamHandler()
handler.setLevel(INFO)
logger.setLevel(INFO)
logger.addHandler(handler)
logger.propagate = False

# マーケット
product = "FX_BTC_JPY"

# timescaleには1, 2, 3 ,4 ,5, 6, 10,12, 15, 20, 30, 60が選択可能 (60を割って割り切れるものだけ)
timescale = 60

class Websocketexecutions:
    def __init__(self, product, timescale):
        self.product = product
        self.timescale = str(timescale)+"s"
        self.channel_executions = "lightning_executions_{}".format(product)
        # 約定履歴を貯めるバッファ。溢れないように余裕をもって多めにしておく
        # 現状の取引量だと秒あたり100～200約定なので、必要量の2.5～5倍程度を確保している
        self.executions = deque(maxlen=timescale*500)
        self.executionsWebsocket()
        warnings.simplefilter(action="ignore", category=FutureWarning)

    # 約定データから datetime 生成
    # 勇者ああああ(@AAAAisBraver)さんのnoteを参考にさせていただきました
    #  https://note.mu/17num/n/naba75f04f386
    def format_date(self, date_line):
        exec_date = date_line.replace('T', ' ')[:-1]
        return dateutil.parser.parse(exec_date) + timedelta(hours=9)

    def updatecandle(self):
        if self.executions :
            start = time.time()

            # dequeをリストに
            tmpExecutions = list(self.executions)

            # ビット鯉(@coibot127)さんのツイートを参考にさせていただいております
            #  https://twitter.com/coibot127/status/1025251540125143045
            self.raw = pandas.DataFrame([[self.format_date(tick["exec_date"]),tick["price"],tick["size"]] for tick in tmpExecutions],columns=["date","price","volume"])
            self.candle=self.raw.set_index('date').resample(self.timescale ,how={"price":"ohlc", "volume":"sum"})
            self.candle.columns=self.candle.columns.droplevel()

            for i in range(len(self.candle)):
                # NaNが自身との等号判定でfalseを返すという性質を利用してNanかどうかを判定
                if self.candle["open"][i]!=self.candle["open"][i]:
                    # NaNの判定 (その期間に約定履歴が無い場合にはひとつ前の足からコピー）
                    self.candle.ix[i,["open","high","low","close"]] =self.candle.ix[i-1,"close"]

            # 変換に要した時間をチェック
            elapsed_time = time.time() - start
            logger.info( "Conversion elapsed_time:{:.3f}".format(elapsed_time) + "[sec]")
            logger.info( "{}ticks -> {}candles".format(len(tmpExecutions),len(self.candle)))

    def executionsWebsocket(self):

        def on_message(ws, message):
            messages = json.loads(message)
            channel = messages["params"]["channel"]
            if channel == self.channel_executions :
                recept_data = messages["params"]["message"]
                for i in recept_data:
                    self.executions.append(i)

        def on_error(ws, error):
            logger.info(error)

        def on_close(ws):
            logger.info("Websocket closed")

        def on_open(ws):
            logger.info("Websocket connected")
            ws.send(json.dumps({"method": "subscribe", "params": {"channel": self.channel_executions}}))

        def run(ws):
            while True:
                ws.run_forever()
                time.sleep(3)

        ws = websocket.WebSocketApp( "wss://ws.lightstream.bitflyer.com/json-rpc",
            on_message=on_message, on_error=on_error, on_close=on_close )
        ws.on_open = on_open
        websocketThread = threading.Thread(target=run, args=(ws, ))
        websocketThread.start()

# ログ切り替えのため翌日の0:00のTimestampを作成
def GenNextday():
    today = datetime.now()
    return pandas.to_datetime( datetime( today.year, today.month, today.day,0, 0, 0)+ timedelta(days=1) )

# 日付名でCSVファイル名を生成
def GenFilename():
    return time.strftime("%Y-%m-%d") + ".csv"

if __name__ == "__main__":

    # Websocketクラスの生成
    websocket = Websocketexecutions( product, timescale )

    # 最初の配信まで待機
    while not websocket.executions :
        time.sleep(1)
    logger.info("Websocket started")

    csvfilename = GenFilename()
    nextday = GenNextday()
    lastdate = ""
    daychanged = False

    while True:
        # 日付が変わったらcsvファイル名を変更
        if daychanged :
            csvfilename = GenFilename()
            daychanged = False
        else:
            # ローソク足3本分程度溜まってから書き込み
            time.sleep(timescale*3)

        # Websocketの中に貯めている約定履歴から最新のローソク足を生成
        websocket.updatecandle()

        # 前回までで未書き込みの足の位置を探す
        lastpos = 0 if lastdate == "" else websocket.candle.index.get_loc(lastdate)

        # 今回書き込む最後の位置を決定
        try:
            # 翌日の0:00 を探す
            endpos = websocket.candle.index.get_loc(nextday)
            # リストの中に翌日の0:00があれば、すでに日付が変わっている
            nextday = GenNextday()
            daychanged = True
        except:
            # 無ければまだ日付は変わっていないので最終足（の1本手前の確定足）までを書き込む
            endpos = len(websocket.candle)-1

        # 追加分だけの足を生成
        latestCandle = websocket.candle[lastpos:endpos]

        # 前回から追加分があればCSVに追記
        if len( latestCandle )>0  :
            logger.info( latestCandle )
            latestCandle.to_csv( csvfilename, header=False, mode='a')

        # 一番最後の足を記憶
        lastdate = websocket.candle[endpos:endpos+1].index[0]

        # 負荷軽減のため、ローソク足に変換済みの約定履歴を半分ほど破棄
        #（変換中に届いた約定履歴を漏らさないように全部は破棄しない）
        count = int(len(websocket.executions)/2)
        for i in range(count):
            websocket.executions.popleft()
