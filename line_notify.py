#!/usr/bin/python3
# coding: utf-8
# Dotenkun Ver. 1.0.2 beta 2
import requests
import datetime
import time
import logging
from requests.exceptions import ConnectionError, HTTPError
from logging import getLogger, Formatter as LogFormatter, StreamHandler as LogStreamHandler, DEBUG as LOG_DEBUG, INFO as LOG_INFO
import logging.handlers
from pytz import timezone, utc
from urllib.error import URLError, HTTPError


# LINE Notify用関数
def line_notify(message, api_token):
    headers = {
        'Authorization':'Bearer ' + api_token
    }
    payload = {
        'message':"\n" + message
    }

    api_url   = 'https://notify-api.line.me/api/notify'
    response = requests.post(api_url ,headers=headers ,params=payload)
    if response.status_code == 200:
        print('LINE Notify 送信成功')
    elif response.status_code == 401:
        print('LINE Notify 認証失敗(Token誤り?)')
    else:
        print(response)

line_notify("trendfilter_range_60がエラー停止","96yzUakFybV8vWf5YWHlCLFjd3XMweWYNwYewJrUPdv")
